#!/usr/bin/python3
import pandas as pd

dataset = pd.read_csv('hackathon_data.csv')
def get_states_with_least_sales():
    months = ['2019-07', '2019-06']

    recent_sales = dataset[dataset['order_date'].str.slice(stop=7).astype(str).isin(months)]
    recent_sales = recent_sales.groupby(['society_id', recent_sales['order_date'].str[:7]]).size().reset_index(name='count')

    prev_month_sales = recent_sales[recent_sales['order_date'].str.slice(stop=7).astype(str) == months[0]]
    prev_month_sales = prev_month_sales.sort_values(by='count')
    societies_with_least_sales = prev_month_sales[:3]['society_id'].values

    worst_recent_sale = recent_sales[recent_sales['society_id'].isin(societies_with_least_sales)]

    data = {}
    for index, row in worst_recent_sale.iterrows():
        if row['society_id'] not in data:
            data[row['society_id']] = [{row['order_date']: row['count']}]
        else:
            data[row['society_id']] += [{row['order_date']: row['count']}]

    recent_sales = prev_month_sales = societies_with_least_sales = worst_recent_sale = None
    print(data)

get_states_with_least_sales()
