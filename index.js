var express = require('express')
var ps = require('python-shell');
var app = express()

app.set('port', (process.env.PORT || 5000))
app.use(express.static(__dirname + '/public'))

app.get('/about', function(request, response) {

  
})

app.get('/product_sale_trend', function(request, response) {
  response.setHeader('Content-Type', 'application/json');
  console.log("Going to run:least_sale ");
  
  ps.PythonShell.run('least_sale.py', {}, function (err, results) {
    console.log("results: "+results);
    console.log("err: "+err);
    response.status(200).send({
        results: results,
        err: err
    });
  });
  
})

app.get('/subscription_analysis', function(request, response) {
  response.setHeader('Content-Type', 'application/json');
  console.log("Going to run:subscription_analysis ");
  ps.PythonShell.run('subscription_analysis.py', {}, function (err, results) {

      console.log("results: "+results);
      console.log("err: "+err);
      response.status(200).send({
        results: results,
        err: err
      });
  });
  
})

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})
