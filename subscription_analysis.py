#!/usr/bin/python3
import pandas as pd

dataset = pd.read_csv('hackathon_data.csv')
#orders_withsubscriptions, witout subscriptions
def subscribtion_sales_analytics():
    current_date = '2019-07-04'
    recent_sales = dataset[dataset['order_date'].str.slice(stop=7).astype(str) == current_date[:7]]
    recent_sales = recent_sales.groupby(['society_id', 'subscription']).size().reset_index(name='subscription_count')

    recent_sales_unsubscribed = recent_sales[recent_sales['subscription'] == 0]
    recent_sales_subscribed = recent_sales[recent_sales['subscription'] == 1]

    recent_sales = None

    recent_sales_subscribed.reset_index(inplace=True, drop=True)
    recent_sales_unsubscribed.reset_index(inplace=True, drop=True)

    recent_sales_unsubscribed = recent_sales_unsubscribed.rename(columns={'subscription_count': 'unsubscription_count', 'subscription': 'unsubscription'})
    recent_sales_unsubscribed = recent_sales_unsubscribed[recent_sales_unsubscribed['unsubscription_count'] > 2000]

    recent_subscription_sales_data = []
    for index, row in recent_sales_unsubscribed.iterrows():
        row = row.values
        subscription_record = recent_sales_subscribed[recent_sales_subscribed['society_id'] == row[0]].values[0]

        temp = {}
        temp['society_id'] = row[0]
        temp['sales_diff'] = row[2] - subscription_record[2]
        temp['subscription_count'] = subscription_record[2]
        temp['unsubscription_count'] = row[2]

        recent_subscription_sales_data.append(temp)

    recent_sales_subscribed = None
    recent_sales_unsubscribed = None

    recent_subscription_sales_data = pd.DataFrame(recent_subscription_sales_data)
    recent_subscription_sales_data = recent_subscription_sales_data.sort_values(by='sales_diff', ascending=False)[:10]

    data = []
    for index, row in recent_subscription_sales_data.iterrows():
        data.append({'society_id': row['society_id'], 'subscription_sales': row['subscription_count'],
                     'non_subscription_sales': row['unsubscription_count']})

    recent_subscription_sales_data = None
    print(data)

subscribtion_sales_analytics()
